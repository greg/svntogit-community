# Maintainer: Felix Yan <felixonmars@archlinux.org>

pkgname=python-hypothesis
pkgver=5.23.3
pkgrel=1
pkgdesc="Advanced Quickcheck style testing library for Python"
arch=('any')
license=('MPL')
url="https://hypothesis.readthedocs.org"
depends=('python-attrs' 'python-sortedcontainers')
optdepends=('python-pytz: for datetime and django module'
            'python-faker: for fakefactory and django module'
            'python-django: for django module'
            'python-numpy: for numpy module'
            'python-pytest: for pytest module'
            'python-lark-parser: for lark module')
makedepends=('python-setuptools')
checkdepends=('python-pytest-runner' 'flake8' 'python-pytz' 'python-numpy' 'python-faker'
              'python-flaky' 'python-django' 'python-mock' 'python-pandas' 'python-dpcontracts'
              'python-pytest-xdist' 'python-lark-parser' 'python-pexpect' 'python-coverage'
              'python-typing_extensions')
source=("$pkgname-$pkgver.tar.gz::https://github.com/HypothesisWorks/hypothesis/archive/hypothesis-python-$pkgver.tar.gz")
sha512sums=('15d4b3b57c234f970bebf7bd98a75a5ac031422b3f70848d25a8ee94852a3f73f8b3e1d69ee850144f1f229d1dbe18fa7959df1930fc42a5768ac7e3d6bb9e7b')

prepare() {
  mv hypothesis-hypothesis-python-$pkgver hypothesis-$pkgver
}

build() {
  cd hypothesis-$pkgver/hypothesis-python
  python setup.py build
}

check() {
  cd hypothesis-$pkgver/hypothesis-python
  python setup.py pytest
}

package() {
  cd hypothesis-$pkgver/hypothesis-python
  python setup.py install --root="$pkgdir" --optimize=1
}
