# Maintainer: Daniel M. Capella <polyzen@archlinux.org>
# Contributor: Sergey A. <murlakatamenka@disroot.org>

pkgname=rust-analyzer
pkgver=20200727
_pkgver=2020-07-27
pkgrel=1
pkgdesc='Experimental Rust compiler front-end for IDEs'
arch=('x86_64')
url=https://github.com/rust-analyzer/rust-analyzer
license=('Apache' 'MIT')
depends=('gcc-libs' 'rust')
source=("$url/archive/$_pkgver/$pkgname-$_pkgver.tar.gz")
sha512sums=('a9dd6d55c09e211e8357077fb94f3704370dcab89adbe6c79faf1b9163b8a5ef902741515961519f5b0eed051d37f264bea6dac797c39f88b17951f01d0182b3')

prepare() {
  cd $pkgname-$_pkgver
  sed -i '/ensure_rustfmt()?;/d' xtask/src/lib.rs
}

pkgver() {
  echo ${_pkgver//-}
}

build() {
  cd $pkgname-$_pkgver
  cargo build --release --locked
}

check() {
  cd $pkgname-$_pkgver
  cargo test --release --locked
}

package() {
  cd $pkgname-$_pkgver
  install -Dt "$pkgdir"/usr/bin target/release/rust-analyzer
  install -Dm644 -t "$pkgdir"/usr/share/licenses/$pkgname LICENSE-MIT
}
