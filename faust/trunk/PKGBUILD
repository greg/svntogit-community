# Maintainer: David Runge <dvzrv@archlinux.org>
# Contributor: Albert Graef <aggraef at gmail.com>
# Contributor: Bernardo Barros <bernardobarros at gmail dot com>
# Contributor: bjoern lindig (bjoern _dot_ lindig _at_ google.com)

pkgname=faust
pkgver=2.27.1
pkgrel=1
pkgdesc="A functional programming language for realtime audio signal processing."
arch=('x86_64')
url="https://faust.grame.fr/"
license=('GPL2')
groups=('pro-audio')
depends=('gcc-libs' 'glibc' 'llvm-libs' 'libmicrohttpd')
makedepends=('cmake' 'libsndfile' 'llvm' 'xxd')
optdepends=('clang: for sound2reader'
            'csound: for faust2csound'
            'gradle: for faust2android'
            'graphviz: for faust2sig{,viewer}'
            'gtk2: for faust2{jack,rosgtk}'
            'inkscape: for faust2{pdf,png}'
            'jack: for faust2{api,jack{,console,internal,server,rust},jaqt,netjackqt,nodejs,ros,rosgtk,rpinetjackconsole}'
            'swig: for faust2android'
            'libsndfile: for faust2{dummy,sndfile}'
            'octave: for faust2octave'
            'pd: for faust2puredata'
            'portaudio: for faust2paqt'
            'python: for faust2md and faust2atomsnippets'
            'qt5-tools: for faust2{faustvst,jaqt,lv2}'
            'rtaudio: for faust2raqt'
            'ruby: for faust2sc and scbuilder'
            'rust: for faust2jackrust'
            'supercollider: for faust2supercollider'
            'texlive-core: for faust2pdf')
provides=('libfaustmachine.so' 'libfaust.so' 'libOSCFaust.so' 'libHTTPDFaust.so')
options=('staticlibs')
source=("https://github.com/grame-cncm/${pkgname}/releases/download/${pkgver}/${pkgname}-${pkgver}.tar.gz"
        "${pkgname}-2.27.1-libmicrohttpd.patch::https://github.com/grame-cncm/faust/commit/afff11d4bdff0ce5bf945e165581de4f5a953520.patch"
)
sha512sums=('feff078784276ae026b76b833b460193c58c3f750fc5d9dc071015c19393d55af4b2d6bf2555d61a19b88ed0e8de9ce82a6785bfaa0754144e3246f52ebc7e4f'
            'efe3fda4f67392763cc631bfaefd91af012ed0978288f237bcb965c19e37b65b99f87f392ee3518a62bbebc98b1d61993a70de68861f19046a7c6ecf47e3dcff')
b2sums=('e6fc33dfe6a7aa0296533f43a6827b06dd39913cc28da2622e1e7eca2e6e342b03e508b10038fea33cd27860cc0e7395d3186eb86ee98d4370663a5dbea1b557'
        '3f87c52493a97049f7b8ea2b1a4f1ed0c7dc3c598a2fd38e77136f9630b41394c360cbf5974d9f66b7d3e64d262182d73586fe8f5a98f27c7e0b9b2220db7d14')

prepare() {
  cd "${pkgname}-${pkgver}"
  # fix build with libmicrohttpd >= 0.9.71
  # https://github.com/grame-cncm/faust/issues/467
  patch -Np1 -i "../${pkgname}-2.27.1-libmicrohttpd.patch"
}

build() {
  cd "${pkgname}-${pkgver}"
  cmake -C build/backends/all.cmake \
        -C build/targets/all.cmake \
        -DCMAKE_INSTALL_PREFIX='/usr' \
        -DCMAKE_BUILD_TYPE='None' \
        -DINCLUDE_DYNAMIC=ON \
        -DINCLUDE_STATIC=ON \
        -DINCLUDE_ITP=ON \
        -W no-dev \
        -B "${pkgname}-build" \
        -S build
  make VERBOSE=1 -C "${pkgname}-build"
  make VERBOSE=1 -C tools/sound2faust
}

package() {
  depends+=('libsndfile.so')
  cd "${pkgname}-${pkgver}"
  make VERBOSE=1 PREFIX=/usr DESTDIR="$pkgdir" install -C "${pkgname}-build"
  make VERBOSE=1 PREFIX=/usr DESTDIR="$pkgdir" install -C tools/sound2faust

  # docs
  install -vDm 644 documentation/{,misc/}*.pdf \
    -t "${pkgdir}/usr/share/doc/${pkgname}" \

  # examples
  install -vd "${pkgdir}/usr/share/${pkgname}/examples"
  cp -vR "examples/"* "${pkgdir}/usr/share/${pkgname}/examples/"

  cd syntax-highlighting
  # atom
  install -vDm 644 "atom/language-${pkgname}/package.json" \
    -t "${pkgdir}/usr/lib/atom/dot-atom/packages/language-${pkgname}/"
  install -vDm 644 "atom/language-${pkgname}/grammars/${pkgname}.cson" \
    -t "${pkgdir}/usr/lib/atom/dot-atom/packages/language-${pkgname}/grammars/"
  install -vDm 644 "atom/language-${pkgname}/settings/language-${pkgname}.cson" \
    -t "${pkgdir}/usr/lib/atom/dot-atom/packages/language-${pkgname}/settings/"
  install -vDm 644 "atom/language-${pkgname}/snippets/"* \
    -t "${pkgdir}/usr/lib/atom/dot-atom/packages/language-${pkgname}/snippets/"
  install -vDm 644 "atom/language-${pkgname}/process-palette.json.linux" \
    "${pkgdir}/usr/share/doc/${pkgname}/process-palette.json"
  # kate
  install -vDm 644 "${pkgname}.xml" \
    -t "${pkgdir}/usr/share/apps/katepart/syntax/"
  # gedit
  install -vDm 644 "${pkgname}.lang" \
    -t "${pkgdir}/usr/share/gtksourceview-2.0/language-specs/"
  install -vDm 644 "${pkgname}.lang" \
    -t "${pkgdir}/usr/share/gtksourceview-3.0/language-specs/"
  # highlight
  install -vDm 644 dsp.lang -t "$pkgdir/usr/share/highlight/langDefs/"
  # nano
  install -vDm 644 "${pkgname}.nanorc" -t "$pkgdir/usr/share/nano/"
  # vim
  install -vDm 644 "${pkgname}.vim" \
    -t "${pkgdir}/usr/share/vim/vimfiles/syntax/"
  # emacs
  install -vDm 644 "${pkgname}-mode.el" \
    -t "${pkgdir}/usr/share/emacs/site-lisp/"

  # removing unusable scripts
  rm -v "${pkgdir}/usr/bin/${pkgname}2au"
  # remove precompiled shared libraries for android:
  # https://github.com/grame-cncm/faust/issues/370
  rm -rvf "${pkgdir}/usr/share/faust/android/app/"{lib,oboe} \
    "${pkgdir}/usr/share/faust/smartKeyboard/android/app/oboe"
}

